Source: linphone-desktop
Section: net
Priority: optional
Maintainer: Debian VoIP Team <pkg-voip-maintainers@lists.alioth.debian.org>
Uploaders: Bernhard Schmidt <berni@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
Build-Depends-Arch:
               libbelcard-dev,
               liblinphone-dev (>= 5.3.105-4),
               libsingleapplication-dev,
               libqt5svg5-dev,
               libxml2-dev,
               pkg-config,
               qtbase5-dev (>= 5.12~),
               qtdeclarative5-dev,
               libqt5texttospeech5-dev,
               patchelf <!cross>,
               qt5keychain-dev,
               qtmultimedia5-dev,
               qtquickcontrols2-5-dev,
               qttools5-dev,
               qttools5-dev-tools
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://www.linphone.org/
Vcs-Browser: https://salsa.debian.org/pkg-voip-team/linphone-stack/linphone-desktop
Vcs-Git: https://salsa.debian.org/pkg-voip-team/linphone-stack/linphone-desktop.git

Package: linphone-desktop
Architecture: any
Depends: linphone-common (>= 5.2.99~),
         qml-module-qt-labs-platform,
         qml-module-qtgraphicaleffects,
         qml-module-qtquick-controls,
         qml-module-qtquick-controls2,
         qml-module-qtquick-dialogs,
         qml-module-qtquick-layouts,
         qml-module-qtquick-shapes,
         qml-module-qtquick-window2,
         qml-module-qtquick2,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: qt5-gtk2-platformtheme
Replaces: linphone (<< 4.0.0~)
Breaks: linphone (<< 4.0.0~)
Description: SIP softphone - graphical client
 Linphone is an audio and video internet phone using the SIP protocol.
 .
 The main features of linphone are:
   - a nice graphical interface;
   - it includes a large variety of codecs with different quality / bandwidths;
   - it uses the well-known and standardised SIP protocol.
 .
 This package includes the Qt/QML based graphical desktop client.
